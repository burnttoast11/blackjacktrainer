/**
 * @fileoverview Contains logic used by dealer to decide whether to hit or stand.
 * @author peder.lindberg@gmail.com (Peder Lindberg)
 */

goog.provide('pederl.blackjack.engine.DealerLogic');

/**
 * Dealer draws a card and says if he is done.
 * @param {pederl.blackjack.Hand} hand Dealers hand.
 * @param {pederl.blackjack.Deck} deck Deck of cards.
 * @returns {boolean} isDone Specifies if dealer is done drawing cards.
 */
pederl.blackjack.engine.DealerLogic.Go = function(hand, deck) {
	var count,
		maxCountUnder21 = 0,
		i;
	
	hand.addCard(deck.getNextCard());
	
	count = hand.getCount();
	
	//Dealer has no aces.
	if(count.length === 1) {
		if(count[0] >= 17) {
			return true;
		} else {
			return false;
		}
	} else {
		for(i = 0; i < count.length; i++) {
			if(count[i] < 21) {
				maxCountUnder21 = count[i];
			}
		}
		if(maxCountUnder21 > 17 && maxCountUnder21 < 22) {
			//Dealer has 18 or higher, don't hit.
			return true;
		} else {
			//Dealer has 17 or less, hit.
			return false;
		}
	}
};