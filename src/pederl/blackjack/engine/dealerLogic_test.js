/**
 * Tests the Deck class.
 */

goog.require('pederl.blackjack.Deck');
goog.require('pederl.blackjack.Card');
goog.require('pederl.blackjack.Hand');
goog.require('pederl.blackjack.engine.DealerLogic');
goog.require('goog.testing.jsunit');

var testGo = function() {
	var deck = new pederl.blackjack.Deck();

	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TWO));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.SIX));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.SIX));
	
	var dealerHand = new pederl.blackjack.Hand();
	dealerHand.addCard(deck.getNextCard());
	dealerHand.addCard(deck.getNextCard());
	var result = pederl.blackjack.engine.DealerLogic.Go(dealerHand, deck);
	assertFalse('Dealer should hit.', result);
	result = pederl.blackjack.engine.DealerLogic.Go(dealerHand, deck);
	assertTrue('Dealer should stand.', result);
};

var testGoSoft17 = function() {
	var deck = new pederl.blackjack.Deck();

	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	
	var dealerHand = new pederl.blackjack.Hand();
	dealerHand.addCard(deck.getNextCard());
	dealerHand.addCard(deck.getNextCard());
	var result = pederl.blackjack.engine.DealerLogic.Go(dealerHand, deck);
	assertFalse('Dealer should hit.', result);
	result = pederl.blackjack.engine.DealerLogic.Go(dealerHand, deck);
	assertTrue('Dealer should stand.', result);
};

var testGoSoft17And20 = function() {
	var deck = new pederl.blackjack.Deck();

	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.FIVE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	
	var dealerHand = new pederl.blackjack.Hand();
	dealerHand.addCard(deck.getNextCard());
	dealerHand.addCard(deck.getNextCard());
	var result = pederl.blackjack.engine.DealerLogic.Go(dealerHand, deck);
	assertFalse('Dealer should hit.', result);
	result = pederl.blackjack.engine.DealerLogic.Go(dealerHand, deck);
	assertTrue('Dealer should stand.', result);
};

var testGoBust = function() {
	var deck = new pederl.blackjack.Deck();

	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	deck.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	
	var dealerHand = new pederl.blackjack.Hand();
	dealerHand.addCard(deck.getNextCard());
	dealerHand.addCard(deck.getNextCard());
	var result = pederl.blackjack.engine.DealerLogic.Go(dealerHand, deck);
	assertFalse('Dealer should hit.', result);
	result = pederl.blackjack.engine.DealerLogic.Go(dealerHand, deck);
	assertTrue('Dealer should stand.', result);
	
	assertTrue('Dealer should be busted.',dealerHand.isBusted());
};