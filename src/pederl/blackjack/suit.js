/**
* @fileoverview The suit of a card.
* @author peder.lindberg@gmail.com (Peder Lindberg)
*/

goog.provide('pederl.blackjack.Suit');

/** 
 * Specifies a suit.
 * @enum {number} */
pederl.blackjack.Suit = {
	HEART: 1,
	SPADE: 2,
	CLUB: 3,
	DIAMOND: 4
};

/** @type {Array.<pederl.blackjack.Suit>} */
pederl.blackjack.Suit.values = [
	pederl.blackjack.Suit.HEART,
	pederl.blackjack.Suit.SPADE,
	pederl.blackjack.Suit.CLUB,
	pederl.blackjack.Suit.DIAMOND
];

