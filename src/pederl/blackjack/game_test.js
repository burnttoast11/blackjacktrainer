/**
 * Tests the Game class.
 */

goog.require('pederl.blackjack.Game');
goog.require('pederl.blackjack.PlayerDecision');
goog.require('goog.testing.jsunit');

var testStart = function() {
	var game = new pederl.blackjack.Game();
	assertTrue('Game should not be null', goog.isDefAndNotNull(game));
	game.start();
	assertTrue('Deck has not been created. Length: ' + game.deck_.getCards().length, game.deck_.getCards().length === 208);
	
	game = new pederl.blackjack.Game();
	game.getDeck().decks_ = 6;
	game.start();
	assertTrue('Deck has not been created.', game.deck_.getCards().length === 312);
};

var testDealHand = function() {
	var game = new pederl.blackjack.Game();
	game.start();
	game.dealHand();
	
	assertTrue('Player was not dealt 2 cards.', game.currentPlayerHand_.getCards().length === 2);
	assertTrue('Computer was not dealt 2 cards.', game.dealerHand_.getCards().length === 2);
};

var testGetGameState = function() {
	var game = new pederl.blackjack.Game();
	game.start();
	game.dealHand();
	
	var data = game.getGameState();
	assertTrue('Player should have 2 cards.', data.playerHands[0].getCards().length === 2);
	assertTrue('Dealer should have 2 cards.', data.dealerHand.getCards().length === 2);
};

var testPlayerTurnHit = function() {
	var game = new pederl.blackjack.Game();
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.EIGHT));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	game.currentPlayerHand_ = new pederl.blackjack.Hand();
	game.playerHands_.push(game.currentPlayerHand_);
	game.currentPlayerHand_.addCard(game.deck_.getNextCard());
	game.currentPlayerHand_.addCard(game.deck_.getNextCard());
	
	var result = game.playerTurn(pederl.blackjack.PlayerDecision.HIT, game.currentPlayerHand_);
	assertTrue('No new hand should be returned.', result === null);
	
	var count = game.currentPlayerHand_.getCount();
	assertTrue('Player should have one possible card count.',count.length === 1);
	assertTrue('Player should have 21.', count[0] === 21);
};

var testPlayerTurnStand = function() {
	var game = new pederl.blackjack.Game();
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.NINE));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	game.currentPlayerHand_ = new pederl.blackjack.Hand();
	game.playerHands_.push(game.currentPlayerHand_);
	game.currentPlayerHand_.addCard(game.deck_.getNextCard());
	game.currentPlayerHand_.addCard(game.deck_.getNextCard());
	
	var result = game.playerTurn(pederl.blackjack.PlayerDecision.STAND, game.currentPlayerHand_);
	assertTrue('No new hand should be returned.', result === null);
	
	var count = game.currentPlayerHand_.getCount();
	assertTrue('Player should have one possible card count.',count.length === 1);
	assertTrue('Player should have 12.', count[0] === 12);
};

var testPlayerTurnSplit = function() {
	var game = new pederl.blackjack.Game();
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.KING));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.EIGHT));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.EIGHT));
	game.currentPlayerHand_ = new pederl.blackjack.Hand();
	game.playerHands_.push(game.currentPlayerHand_);
	game.currentPlayerHand_.addCard(game.deck_.getNextCard());
	game.currentPlayerHand_.addCard(game.deck_.getNextCard());
	
	var result = game.playerTurn(pederl.blackjack.PlayerDecision.SPLIT, game.currentPlayerHand_);
	assertTrue('New hand should be returned.', result !== null);
	assertTrue('', game.playerHands_.length === 2);
	
	var count = game.currentPlayerHand_.getCount();
	assertTrue('Player should have one possible card count.',count.length === 1);
	assertTrue('Player should have 18. Count: ' + count[0], count[0] === 18);
	assertTrue('Hand 2 should have 11 Count: ' + result.getCount()[0], result.getCount()[0] === 11);
	
	count = result.getCount();
	assertTrue('Player should have one possible card count.',count.length === 1);
	assertTrue('Player should have 11.', count[0] === 11);
	
	var nextResult = game.playerTurn(pederl.blackjack.PlayerDecision.HIT, result);
	assertTrue('No new hand should be returned.', nextResult === null);
	
	count = result.getCount();
	assertTrue('Player should have one possible card count.',count.length === 1);
	assertTrue('Player should have 21. Count: ' + count[0], count[0] === 21);
};

var testPlayerTurnDouble = function() {
	var game = new pederl.blackjack.Game();
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.EIGHT));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.THREE));
	game.currentPlayerHand_ = new pederl.blackjack.Hand(10);
	game.playerHands_.push(game.currentPlayerHand_);
	game.currentPlayerHand_.addCard(game.deck_.getNextCard());
	game.currentPlayerHand_.addCard(game.deck_.getNextCard());
	
	var result = game.playerTurn(pederl.blackjack.PlayerDecision.DOUBLE, game.currentPlayerHand_);
	assertTrue('No new hand should be returned.', result === null);
	
	var count = game.currentPlayerHand_.getCount();
	assertTrue('Player should have one possible card count.',count.length === 1);
	assertTrue('Player should have 21.', count[0] === 21);
	assertTrue('Player should have bet of 20.', game.currentPlayerHand_.getBet() === 20);
};

var testPlayHandSimpleWin = function() {
	var game = new pederl.blackjack.Game();
	game.start();
	
	//Rig the deck.
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.EIGHT));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	
	game.dealHand();
	
	var data = game.getGameState();
	assertTrue('Player should have 2 cards.', data.playerHands[0].getCards().length === 2);
	assertTrue('Dealer should have 2 cards.', data.dealerHand.getCards().length === 2);
	
	assertTrue('Player should have 20. Count: ' + game.currentPlayerHand_.getCount()[0], game.currentPlayerHand_.getCount()[0] === 20);
	assertTrue('Dealer should have 18.', game.dealerHand_.getCount()[0] === 18);
	
	game.handFinished();
	assertTrue('Player should have won.', data.playerHands[0].getWinner() === true);
};

var testPlayHandPush = function() {
	var game = new pederl.blackjack.Game();
	game.start();
	
	//Rig the deck.
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	
	game.dealHand();
	
	var data = game.getGameState();
	assertTrue('Player should have 2 cards.', data.playerHands[0].getCards().length === 2);
	assertTrue('Dealer should have 2 cards.', data.dealerHand.getCards().length === 2);
	
	assertTrue('Player should have 20. Count: ' + game.currentPlayerHand_.getCount()[0], game.currentPlayerHand_.getCount()[0] === 20);
	assertTrue('Dealer should have 20.', game.dealerHand_.getCount()[0] === 20);
	
	game.handFinished();
	assertTrue('Player should have pushed.', data.playerHands[0].getWinner() === null);
};

var testPlayHandSimpleLoss = function() {
	var game = new pederl.blackjack.Game();
	game.start();
	
	//Rig the deck.
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.EIGHT));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	game.deck_.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TEN));
	
	game.dealHand();
	
	var data = game.getGameState();
	assertTrue('Player should have 2 cards.', data.playerHands[0].getCards().length === 2);
	assertTrue('Dealer should have 2 cards.', data.dealerHand.getCards().length === 2);
	
	assertTrue('Player should have 18. Count: ' + game.currentPlayerHand_.getCount()[0], game.currentPlayerHand_.getCount()[0] === 18);
	assertTrue('Dealer should have 20.', game.dealerHand_.getCount()[0] === 20);
	
	game.handFinished();
	assertTrue('Player should have lost.', data.playerHands[0].getWinner() === false);
};