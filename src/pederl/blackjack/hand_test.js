/**
 * Tests the Hand class.
 */

goog.require('goog.testing.jsunit');
goog.require('pederl.blackjack.Hand');

var testIsBusted = function() {
	var hand = new pederl.blackjack.Hand();

	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.FIVE));
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.QUEEN));
	
	assertFalse('Hand is not busted.', hand.isBusted());
	
	hand.cards_.push();
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.JACK));
	assertTrue('Hand is busted', hand.isBusted());
};

var testCountHand = function() {
	var hand = new pederl.blackjack.Hand(),
		count;
	
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.FIVE));
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.JACK));
	
	count = hand.getCount();
	assertTrue('There should be one possible card count.', count.length === 1);
	assertTrue('Card count should be 15. Count: ' + count[0], count[0] === 15);
};

var testCountHandWithAce = function() {
	var hand = new pederl.blackjack.Hand(),
		count;
	
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.FIVE));
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	
	count = hand.getCount();
	assertTrue('There should be two possible card counts. Counts: ' + count.length, count.length === 2);
	assertTrue('Card count 1 should be 6. Count: ' + count[0], count[0] === 6);
	assertTrue('Card count 2 should be 16. Count: ' + count[1], count[1] === 16);
};

var testCountHandWithTwoAces = function() {
	var hand = new pederl.blackjack.Hand(),
		count;
	
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.SIX));
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	
	count = hand.getCount();
	assertTrue('There should be 3 possible card counts. Counts: ' + count.length, count.length === 3);
	assertTrue('Card count 1 should be 8. Count: ' + count[0], count[0] === 8);
	assertTrue('Card count 2 should be 18. Count: ' + count[1], count[1] === 18);
	assertTrue('Card count 3 should be 28. Count: ' + count[2], count[2] === 28);
};

var testCountHandWithThreeAces = function() {
	var hand = new pederl.blackjack.Hand(),
		count;
	
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.TWO));
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	hand.cards_.push(new pederl.blackjack.Card(pederl.blackjack.Suit.HEART, pederl.blackjack.CardValue.ACE));
	
	count = hand.getCount();
	assertTrue('There should be 4 possible card counts. Counts: ' + count.length, count.length === 4);
	assertTrue('Card count 1 should be 5. Count: ' + count[0], count[0] === 5);
	assertTrue('Card count 2 should be 15. Count: ' + count[1], count[1] === 15);
	assertTrue('Card count 3 should be 25. Count: ' + count[2], count[2] === 25);
	assertTrue('Card count 4 should be 35. Count: ' + count[3], count[3] === 35);
};