/**
 * @fileoverview Represents the deck of cards being used for this game of blackjack.
 * @author peder.lindberg@gmail.com (Peder Lindberg)
 */

goog.provide('pederl.blackjack.Deck');

goog.require('pederl.blackjack.Suit');
goog.require('pederl.blackjack.CardValue');
goog.require('pederl.blackjack.Card');
goog.require('goog.debug.Logger');
goog.require('goog.debug.Logger.Level');
goog.require('goog.debug');
goog.require('goog.debug.FancyWindow');

/**
 * Creates a new deck of cards.
 * @param {number=} decks Number of decks.
 * @constructor
 */
pederl.blackjack.Deck = function(decks) {
	if(goog.isDef(decks)) {
		this.decks_ = decks;
	}
	
	/**
	 * @type {Array.<pederl.blackjack.Card>}
	 * @private
	 */
	this.cards_ = [];
	/**
	 * @type {Array.<pederl.blackjack.Card>}
	 * @private
	 */
	this.playedCards_ = [];
	
	/**
	 * Returns true when deck is so small that we need to reshuffle.
	 * @returns {boolean}
	 */
	this.cardsLow = function() {
		return this.cards_.length <= pederl.blackjack.Deck.MINCARDS;
	};
};
goog.exportSymbol('pederl.blackjack.Deck', pederl.blackjack.Deck);

/**
 * When number of cards remaining in deck are less than or equal to this value,
 * the deck must be reshuffled.
 * @const
 * @type {number}
 */
pederl.blackjack.Deck.MINCARDS = 15;

/**
 * The number of decks in the shoe. 
 * @type {number}
 * @private
*/
pederl.blackjack.Deck.prototype.decks_ = 4;

/**
 * Logger for pederl.blackjack.Deck.
 * @type {goog.debug.Logger}
 */
pederl.blackjack.Deck.prototype.logger_ = goog.debug.Logger.getLogger('pederl.blackjack.Deck');

/**
 * Returns the next card in the deck.
 * @return {pederl.blackjack.Card}
 */
pederl.blackjack.Deck.prototype.getNextCard = function() {
	var card = this.cards_.pop();
	this.playedCards_.push(card);
	return card;
};

/**
 * Creates a new shuffled deck.
 */
pederl.blackjack.Deck.prototype.shuffle = function() {
    // Create the debug window.
//    var debugWindow = new goog.debug.FancyWindow('main');
//    debugWindow.setEnabled(true);
//    debugWindow.init();
    
	var i,
		j,
		k,
		index = 0,
		tempCard;

	for(i = 0; i < this.decks_; i++) {
		for(j = 0; j < 4; j++) {
			for(k = 0; k < 13; k++) {
				this.cards_[index] = new pederl.blackjack.Card(pederl.blackjack.Suit.values[j], 
						pederl.blackjack.CardValue.values[k]);
				index++;
			}
		}
	}
	
    for (i = this.cards_.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        tempCard = this.cards_[i];
        this.cards_[i] = this.cards_[j];
        this.cards_[j] = tempCard;
    }
};
goog.exportSymbol('pederl.blackjack.Deck.prototype.shuffle', pederl.blackjack.Deck.prototype.shuffle);

/**
 * Returns all of the cards in this deck.
 * @return Array.<pederl.blackjack.Card>
 */
pederl.blackjack.Deck.prototype.getCards = function(){
	return this.cards_;
};

/**
 * Returns the number of decks being used at this black jack table.
 * @returns {number}
 */
pederl.blackjack.Deck.prototype.getNumberOfDecks = function() {
	return this.decks_;
};