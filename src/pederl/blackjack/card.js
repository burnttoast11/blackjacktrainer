/**
* @fileoverview A single card in the deck.
* @author peder.lindberg@gmail.com (Peder Lindberg)
*/

goog.provide('pederl.blackjack.Card');

goog.require('pederl.blackjack.Suit');
goog.require('pederl.blackjack.CardValue');

/**
* Creates a new Card.
* @param {pederl.blackjack.Suit} suit Suit of this Card.
* @param {pederl.blackjack.CardValue} value Value of this Card.
* @constructor
*/
pederl.blackjack.Card = function(suit, value) {
	/**
	* @type {pederl.blackjack.Suit}
	* @private
	*/
	this.suit_ = suit;
	/**
	* @type {pederl.blackjack.CardValue}
	* @private
	*/
	this.value_ = value;
};

/**
 * The suit of this card.
 * @returns {pederl.blackjack.Suit}
 */
pederl.blackjack.Card.prototype.getSuit = function() {
	return this.suit_;
};

/**
 * The value of this card.
 * @returns {pederl.blackjack.CardValue}
 */
pederl.blackjack.Card.prototype.getValue = function() {
	return this.value_;
};

/**
 * The actual numerical value of this card.
 * @returns {number}
 */
pederl.blackjack.Card.prototype.getNumbericValue = function() {
	switch(this.value_) {
		case pederl.blackjack.CardValue.TWO:
			return 2;
		case pederl.blackjack.CardValue.THREE:
			return 3;
		case pederl.blackjack.CardValue.FOUR:
			return 4;
		case pederl.blackjack.CardValue.FIVE:
			return 5;
		case pederl.blackjack.CardValue.SIX:
			return 6;
		case pederl.blackjack.CardValue.SEVEN:
			return 7;
		case pederl.blackjack.CardValue.EIGHT:
			return 8;
		case pederl.blackjack.CardValue.NINE:
			return 9;
		case pederl.blackjack.CardValue.TEN:
			return 10;
		case pederl.blackjack.CardValue.JACK:
			return 10;
		case pederl.blackjack.CardValue.QUEEN:
			return 10;
		case pederl.blackjack.CardValue.KING:
			return 10;
		case pederl.blackjack.CardValue.ACE:
			return 11;
		default:
			return 0;
	}
};