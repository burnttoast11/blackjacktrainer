/**
* @fileoverview The value of a card.
* @author peder.lindberg@gmail.com (Peder Lindberg)
*/

goog.provide('pederl.blackjack.CardValue');

/** 
 * Specifies the value of a card.
 * @enum {number} */
pederl.blackjack.CardValue = {
	ACE: 1,
	TWO: 2,
	THREE: 3,
	FOUR: 4,
	FIVE: 5,
	SIX: 6,
	SEVEN: 7,
	EIGHT: 8,
	NINE: 9,
	TEN: 10,
	JACK: 11,
	QUEEN: 12,
	KING: 13
};

/** @type {Array.<pederl.blackjack.CardValue>} */
pederl.blackjack.CardValue.values = [
	pederl.blackjack.CardValue.ACE,
	pederl.blackjack.CardValue.TWO,
	pederl.blackjack.CardValue.THREE,
	pederl.blackjack.CardValue.FOUR,
	pederl.blackjack.CardValue.FIVE,
	pederl.blackjack.CardValue.SIX,
	pederl.blackjack.CardValue.SEVEN,
	pederl.blackjack.CardValue.EIGHT,
	pederl.blackjack.CardValue.NINE,
	pederl.blackjack.CardValue.TEN,
	pederl.blackjack.CardValue.JACK,
	pederl.blackjack.CardValue.QUEEN,
	pederl.blackjack.CardValue.KING
];