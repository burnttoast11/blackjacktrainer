/**
 * @fileoverview Represents the hand of cards held by a single player.
 * @author peder.lindberg@gmail.com (Peder Lindberg)
 */

goog.provide('pederl.blackjack.Hand');

goog.require('pederl.blackjack.Card');

/**
 * Creates a new hand.
 * @param {number=} bet Bet placed on this hand.
 * @constructor
 */
pederl.blackjack.Hand = function(bet) {
	/**
	 * The cards in this hand.
	 * @type {Array.<pederl.blackjack.Card>}
	 * @private
	 */
	this.cards_ = [];
	
	if(goog.isDef(bet)) {
		this.bet_ = bet;
	}
	
	/**
	 * Specifies if this hand won.
	 * @type {boolean|null}
	 * @private
	 */
	this.winner_ = null;
};

/**
 * @type {number} Bet placed on this hand.
 * @private
 */
pederl.blackjack.Hand.prototype.bet_ = 0;

/**
 * Specifies if this hand won.
 * @returns {boolean|null}
 */
pederl.blackjack.Hand.prototype.getWinner = function() {
	return this.winner_;
};

/**
 * Set whether this hand won.
 * @param {boolean|null} won
 */
pederl.blackjack.Hand.prototype.setWinner = function(won) {
	this.winner_ = won;
};

/**
 * Adds a card to this hand.
 * @param {pederl.blackjack.Card} card Card to add to this hand.
 */
pederl.blackjack.Hand.prototype.addCard = function(card) {
	this.cards_.push(card);
};

/**
 * Specifies is hand is busted.
 * @return {boolean}
 */
pederl.blackjack.Hand.prototype.isBusted = function() {
	if(this.getCount() > 21) {
		return true;
	}
	return false;
};

/**
 * Gets cards in this hand.
 * @returns {Array.<pederl.blackjack.Card>}
 */
pederl.blackjack.Hand.prototype.getCards = function() {
	return this.cards_;
};

/**
 * Clears the cards from this hand.
 */
pederl.blackjack.Hand.prototype.clear = function() {
	this.cards_ = [];
};

/**
 * Returns the vale of the bet placed on this hand.
 * @returns {number}
 */
pederl.blackjack.Hand.prototype.getBet = function() {
	return this.bet_;
};

/**
 * Double the value of the bet on this hand. This does not change the player's chip count.
 */
pederl.blackjack.Hand.prototype.doubleDown = function() {
	this.bet_ *= 2;
};

/**
 * The final value of this hand.
 * @returns {number}
 */
pederl.blackjack.Hand.prototype.getFinalCount = function() {
	var i,
		counts = this.getCount(),
		finalCount = 0,
		max = 0;
	
	for(i = 0; i < counts.length; i++) {
		if(counts[i] < 22 && counts[i] > finalCount) {
			finalCount = counts[i];
		}
		if(counts[i] > max) {
			max = counts[i];
		}
	}
	
	if(finalCount === 0) {
		finalCount = max;
	}
	
	return finalCount;
};

/**
 * Gets the value of the hand.
 * @return {Array.<number>}
 */
pederl.blackjack.Hand.prototype.getCount = function() {
	var i,
		j,
		count = [0],
		value,
		countCopy;
	
	if(this.cards_.length === 0) {
		return count;
	} else {
		for(i = 0; i < this.cards_.length; i++) {
			value = this.cards_[i].getValue();
			if(value === pederl.blackjack.CardValue.ACE) {
				//We now have twice as many possibilities. Clone array and add 1 and 11 respectively.
				countCopy = [];
				
				//Make a copy of the current count array.
				for(j = 0; j < count.length; j++) {
					countCopy.push(JSON.parse(JSON.stringify(count[j])));
				}
				this.constructor.addToPossibleCounts(count, 1);
				this.constructor.addToPossibleCounts(countCopy, 11);
				count = this.constructor.unionCounts(count, countCopy);
				
			} else if(value > 10){
				this.constructor.addToPossibleCounts(count, 10);
			} else {
				this.constructor.addToPossibleCounts(count, value);
			}
			
		}
	}
	
	return count;
};

/**
 * Adds a value to all counts in an array.
 * @private
 */
pederl.blackjack.Hand.addToPossibleCounts = function(counts, value) {
	var i;
	for(i = 0; i < counts.length; i++) {
		counts[i] += value;
	}
};

/**
 * Returns an array of unique counts.
 * @private
 * @return {Array.<number>}
 */
pederl.blackjack.Hand.unionCounts = function(x, y) {
	  var obj = {};
	  for (var i = x.length-1; i >= 0; -- i)
	     obj[x[i]] = x[i];
	  for (var i = y.length-1; i >= 0; -- i)
	     obj[y[i]] = y[i];
	  var res = [];
	  for (var k in obj) {
		  if (obj.hasOwnProperty(k)) {
			  res.push(obj[k]);
		  }
	  }
	  return res;
};