/**
* @fileoverview Represents the state of the game.
* @author peder.lindberg@gmail.com (Peder Lindberg)
*/

goog.provide('pederl.blackjack.GameState');

goog.require('pederl.blackjack.Card');
goog.require('pederl.blackjack.Deck');

/**
* Creates a new GameState.
* @param {pederl.blackjack.Deck} deck Deck being used for this game.
* @constructor
*/
pederl.blackjack.GameState = function(deck) {
	/**
	 * Deck being used for this game.
	 * @type {pederl.blackjack.Deck}
	 * @private
	 */
	this.deck_ = deck;
	
	/**
	 * Player's hand.
	 * @type {Array.<pederl.blackjack.Hand>}
	 */
	this.playerHands;
	
	/**
	 * Dealer's hand.
	 * @type {pederl.blackjack.Hand}
	 */
	this.dealerHand;
	
	/**
	 * @type {Array.<Array.<number>>}
	 * @private
	 */
	this.playerCounts = [];
	
	/**
	 * @type {Array.<number>}
	 * @private
	 */
	this.dealerCount = [];
};

/**
 * Current player card count for each player hand.
 * @return {Array.<Array.<number>>}
 */
pederl.blackjack.GameState.prototype.getPlayerCounts = function() {
	return this.playerCounts;
};

/**
 * Current player card count.
 * @return {Array.<number>}
 */
pederl.blackjack.GameState.prototype.getDealerCount = function() {
	return this.dealerCount;
};

/**
 * Updates the GameState.
 * @param {Array.<pederl.blackjack.Hand>} playerHands Reference to player's hand.
 * @param {pederl.blackjack.Hand} dealerHand Reference to dealer's hand.
 */
pederl.blackjack.GameState.prototype.update = function(playerHands, dealerHand) {
	var i;
	
	this.playerHands = playerHands;
	this.dealerHand = dealerHand;
	for(i = 0; i < this.playerCounts.length; i++) {
		this.playerCounts.push(playerHands[i].getCount());
	}
	this.dealerCount = dealerHand.getCount();	
};

pederl.blackjack.GameState.prototype.finish = function(playerHands, dealerHand) {
	var dealerBusted = dealerHand.isBusted(),
		i;
	this.update(playerHands, dealerHand);
	
	for(i = 0; i < playerHands.length; i++) {
		if(playerHands[i].isBusted() === true) {
			playerHands[i].setWinner(false);
		} else if(dealerBusted === true || dealerHand.getFinalCount() < playerHands[i].getFinalCount()) {
			playerHands[i].setWinner(true);
		} else if(dealerHand.getFinalCount() === playerHands[i].getFinalCount()){
			playerHands[i].setWinner(null);
		} else {
			playerHands[i].setWinner(false);
		}
	}
};