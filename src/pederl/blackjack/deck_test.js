/**
 * Tests the Deck class.
 */

goog.require('pederl.blackjack.Deck');
goog.require('goog.testing.jsunit');

var testShuffle = function() {
	var deck = new pederl.blackjack.Deck(),
		d = deck.getCards(),
		arr = [],
		i,
		num,
		numDecks;
	
	deck.shuffle();
	assertTrue('Should be 208 cards in deck. Actual: ' + d.length, d.length == 208);
	numDecks = deck.getNumberOfDecks();
	
	for(i = 0; i < d.length; i++) {
		if(d[i].getValue() === pederl.blackjack.CardValue.JACK) {
			num = (d[i].getSuit() - 1) * 13 + 11;
		} else if(d[i].getValue() === pederl.blackjack.CardValue.QUEEN) {
			num = (d[i].getSuit() - 1) * 13 + 12;
		} else if(d[i].getValue() === pederl.blackjack.CardValue.KING) {
			num = (d[i].getSuit() - 1) * 13 + 13;
		} else {
			num = (d[i].getSuit() - 1) * 13 + d[i].getValue();
		}
		if(goog.isDefAndNotNull(arr[num - 1])) {
			arr[num - 1]++;
		} else {
			arr[num - 1] = 1;
		}
	}
	
	assertTrue('Arr should have length 52. Length: ' + arr.length, arr.length === 52);
	for(i = 1; i < arr.length; i++) {
		assertTrue('Arr index = ' + i + ' There should be ' + numDecks 
				+ ' of each card. There was only ' + arr[i] 
				+ ' for Suit: ' + d[i].getSuit() + ' Value: ' + d[i].getValue(), arr[i] === numDecks);
	}
};

var testNumberOfDecks = function() {
	var deck = new pederl.blackjack.Deck(6),
		d = deck.getCards();
	
	deck.shuffle();
	assertTrue('Should be 312 cards in deck. Actual: ' + d.length, d.length == 312);
};

var testNumberOfDecks = function() {
	var deck = new pederl.blackjack.Deck(),
		d = deck.getCards(),
		i;
	
	deck.shuffle();
	assertTrue('Should be 208 cards in deck. Actual: ' + d.length, d.length == 208);
	
	for(i = 0; i < 208; i++) {
		deck.getNextCard();
	}
	
	assertTrue('Should be 208 played cards.', deck.playedCards_.length === 208);
};
