/**
 * @fileoverview Represents the black jack game.
 * @author peder.lindberg@gmail.com (Peder Lindberg)
 */

goog.provide('pederl.blackjack.Game');

goog.require('pederl.blackjack.Deck');
goog.require('pederl.blackjack.Hand');
goog.require('pederl.blackjack.GameState');
goog.require('pederl.blackjack.PlayerDecision');

/**
 * Represents a game of black jack.
 * @param {number} playerChips Player's total chip count.
 * @constructor
 */
pederl.blackjack.Game = function(playerChips) {
	if(goog.isDef(playerChips)) {
		this.playerChips_ = playerChips;
	}
	
	/**
	 * Reference to the deck.
	 * @type {pederl.blackjack.Deck}
	 * @private
	 */
	this.deck_ = new pederl.blackjack.Deck();
	
	/**
	 * Player's hands.
	 * @type {Array.<pederl.blackjack.Hand>}
	 * @private
	 */
	this.playerHands_ = [];
	
	/**
	 * The current player hand. (Splitting can give more than one hand.)
	 * @type {pederl.blackjack.Hand}
	 * @private
	 */
	this.currentPlayerHand_;
	
	/**
	 * Dealer's hand.
	 * @type {pederl.blackjack.Hand}
	 * @private
	 */
	this.dealerHand_ = new pederl.blackjack.Hand();
	
	/**
	 * The current state of this game.
	 * @type {pederl.blackjack.GameState}
	 * @private
	 */
	this.gameState_ = new pederl.blackjack.GameState(this.deck_);
};
goog.exportSymbol('pederl.blackjack.Game', pederl.blackjack.Game);

/**
 * @type {number} Player chip count.
 */
pederl.blackjack.Game.prototype.playerChips_ = 200;

/**
 * Starts the game.
 */
pederl.blackjack.Game.prototype.start = function() {
	this.deck_.shuffle();
};
goog.exportSymbol('pederl.blackjack.Game.prototype.start', pederl.blackjack.Game.prototype.start);

/**
 * Deals cards to all players in the game.
 */
pederl.blackjack.Game.prototype.dealHand = function() {
	this.playerHands_ = [new pederl.blackjack.Hand()];
	this.dealerHand_.clear();
	this.currentPlayerHand_ = this.playerHands_[0];
	
	this.currentPlayerHand_.addCard(this.deck_.getNextCard());
	this.dealerHand_.addCard(this.deck_.getNextCard());
	this.currentPlayerHand_.addCard(this.deck_.getNextCard());
	this.dealerHand_.addCard(this.deck_.getNextCard());
};
//goog.exportSymbol('pederl.blackjack.Game.prototype.dealHand', pederl.blackjack.Game.prototype.dealHand);

/**
 * @param {pederl.blackjack.PlayerDecision} decision The player's next move.
 * @param {pederl.blackjack.Hand} playerHand The player's hand.
 * @returns {pederl.blackjack.Hand|null} Returns null unless player has split.
 */
pederl.blackjack.Game.prototype.playerTurn = function(decision, playerHand) {
	var newHand = null;
	switch(decision) {
		case pederl.blackjack.PlayerDecision.STAND:
			break;
		case pederl.blackjack.PlayerDecision.HIT:
			playerHand.addCard(this.deck_.getNextCard());
			break;
		case pederl.blackjack.PlayerDecision.SPLIT:
			newHand = new pederl.blackjack.Hand();
			newHand.addCard(playerHand.getCards().pop());
			playerHand.addCard(this.deck_.getNextCard());
			newHand.addCard(this.deck_.getNextCard());
			this.playerHands_.push(newHand);
			break;
		case pederl.blackjack.PlayerDecision.DOUBLE:
			playerHand.doubleDown();
			this.playerChips_ -= playerHand.getBet();
			playerHand.addCard(this.deck_.getNextCard());
			break;
	}
	
	return newHand;
};

/**
 * Gets the deck.
 * @return {pederl.blackjack.Deck} 
 */
pederl.blackjack.Game.prototype.getDeck = function() {
	return this.deck_;
};
//goog.exportSymbol('pederl.blackjack.Game.prototype.getDeck', pederl.blackjack.Game.prototype.getDeck);

/**
 * Gets the current state of this game.
 * @return {pederl.blackjack.GameState}
 */
pederl.blackjack.Game.prototype.getGameState = function() {
	this.gameState_.update(this.playerHands_, this.dealerHand_);
	return this.gameState_;
};

pederl.blackjack.Game.prototype.handFinished = function() {
	this.gameState_.finish(this.playerHands_, this.dealerHand_);
};