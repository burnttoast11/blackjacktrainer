/**
* @fileoverview Player's decision.
* @author peder.lindberg@gmail.com (Peder Lindberg)
*/

goog.provide('pederl.blackjack.PlayerDecision');

/** 
 * Specifies the player's decision.
 * @enum {number} */
pederl.blackjack.PlayerDecision = {
	HIT: 0,
	STAND: 1,
	SPLIT: 2,
	DOUBLE: 3
};